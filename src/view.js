
export function showGameEndedContainer(playerId, players, moves) {
  const score = document.createElement('div');
  const sortedPlayers = players.sort((a, b) => b.score - a.score );

  const highestScore = sortedPlayers[0].score;

  sortedPlayers.forEach(player => {
    const playerScore = document.createElement('div');
    const bgClass = player.score === highestScore ? 'bg-success' : 'bg-info';
    playerScore.classList.add("col", "text-center", "m-1", "p-2", bgClass);
    playerScore.innerHTML = `${player.id} <br/> Score: ${player.score}`;
    score.appendChild(playerScore);
  });

  if (sortedPlayers[0].id == playerId) {
    addMessage(score, "You won!", "success");
  } else {
    addMessage(score, "You lost :(", "info");
  }

  addTurnTable(score, moves);

  _getContainer().appendChild(score);
}

export function addMoveToTurnTable(move, onClickFn) {
  const turnTable = document.getElementById('turn-table');
  const row = document.createElement('tr');
  _addEntryToRow(row, move.card1.id);
  _addEntryToRow(row, move.card2.id);
  _addEntryToRow(row, move.matched);
  const rowEntry = document.createElement('td');
  const button = document.createElement("button");
  button.classList.add("btn", "btn-warning", "btn-sm");
  button.innerHTML = "Revert to this move";
  button.id = move.moveId;
  button.addEventListener ("click", onClickFn);
  rowEntry.appendChild(button);
  row.appendChild(rowEntry);

  turnTable.appendChild(row);
}

export function addTurnTable(element) {
  const table = document.createElement('table');
  table.classList.add("table", "table-sm");
  table.id="turn-table";

  if (element) {
    element.appendChild(table);
  } else {
    _getGameContainer().appendChild(table);
  }
}

function _addEntryToRow(row, text) {
  const rowEntry = document.createElement('td');
  rowEntry.innerHTML = text;
  row.appendChild(rowEntry);
}

export function createGameContainer() {
  const game = document.createElement('div');
  game.id="game";

  _getContainer().appendChild(game);

  return game;
}

function _getGameContainer() {
  return document.getElementById('game');
}

export function createPendingGameContainer(playerId, onClickFn) {
  const game = document.createElement('div');
  game.innerHTML = "Players";
  addMessage(game, "Waiting for players to join...", "info");

  const button = createButton("Start Game", onClickFn);
  const playerList = document.createElement('ul');
  playerList.id = 'playerList';
  game.appendChild(playerList);
  game.appendChild(button);

  const container = _getContainer();
  container.innerHTML = "";
  container.prepend(game);

  addPlayerToWaitingPlayerList(playerId);
}

export function addPlayerToWaitingPlayerList(playerId) {
  const playerList = document.getElementById('playerList');
  const playerEntry = document.createElement('li');
  playerEntry.innerHTML = playerId;
  playerList.appendChild(playerEntry);
}

export function createCardElement(card, onCardClick) {
  const cardElement = document.createElement("div");
  cardElement.id=card.id;
  cardElement.addEventListener("click", onCardClick);
  cardElement.classList.add("col", "bg-primary", "m-1", "p-2", "text-center");
  cardElement.innerHTML = card.id;

  return cardElement;
}

export function disableCard(card) {
  const cardElement = _getCardElement(card.id);
  cardElement.innerHTML = "&zwnj;"; // empty char to keep size
  cardElement.classList.remove("bg-success", "bg-primary");
}

export function turnCard(card) {
  const cardElement = _getCardElement(card.id);
  cardElement.innerHTML = card.id;
  cardElement.classList.remove("bg-info");
  cardElement.classList.add("bg-primary");
}

export function showCard(card, matched) {
  const cardElement = _getCardElement(card.id);
  cardElement.innerHTML = card.value;
  const bgClass = matched ? "bg-success" : "bg-info";
  cardElement.classList.remove("bg-primary", "bg-info");
  cardElement.classList.add(bgClass);
}

export function _getCardElement(cardId) {
  return document.getElementById(cardId);
}

export function addGameToList(id, onClickFn) {
  const gameEntry = document.createElement('div');
  gameEntry.innerHTML = `Game ${id}`;
  const button = createButton("Join Game", onClickFn);
  gameEntry.appendChild(button);
  _getGameList().prepend(gameEntry);
}

function createButton(text, onClickFn) {
  const button = document.createElement("button");
  button.classList.add("btn", "btn-primary");
  button.innerHTML = text;
  button.addEventListener ("click", onClickFn);
  return button;
}

export function createContainer(createGameFn) {
  const container = document.createElement('div');
  container.id="container";
  container.classList.add("container");

  const gamelist = document.createElement('div');
  gamelist.id='gamelist';
  container.appendChild(gamelist);

  const button = createButton("Create Game", createGameFn);
  container.appendChild(button);

  return container;
}

export function addRowToGame() {
  const game = _getGameContainer();
  const row = document.createElement("div");
  row.classList.add("row");
  game.appendChild(row);
  return row;
}

export function removeMessage() {
  const messageElement = document.getElementById('message');
  if (messageElement) {
    messageElement.parentNode.removeChild(messageElement);
  }
}

export function addMessage(element, message, level) {
  const messageElement = document.createElement('div');
  messageElement.id = 'message';
  messageElement.classList.add('alert', `alert-${level}`);
  messageElement.innerHTML = message;
  if (element) {
    element.prepend(messageElement);
  } else {
    _getContainer().prepend(messageElement);
  }
}

function _getGameList() {
  return document.getElementById('gamelist');
}

function _getContainer() {
  const container = document.getElementById('container');
  container.innerHTML = "";
  return container;
}
