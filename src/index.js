require('file-loader?name=[name].[ext]!./index.html');

import * as view from './view';

// const WEBSOCKET_URL = "ws://192.168.1.65:3000";
const WEBSOCKET_URL = "ws://localhost:3000";
const CARD_REVEAL_TIME = 2000;
const DEBUG = false;
const playerId = 'player-' + Math.floor(Math.random() * 1000 + 1);

let cards;
let gameId;
const selectedCards = [];

function onGameEnded(context) {
  const { players, moves } = context;

  view.showGameEndedContainer(playerId, players, moves);

  moves.forEach(move => view.addMoveToTurnTable(move, onMoveResetClick));
}

function onGameStarted(context) {
  cards = context.cards;
  gameId = context.id;

  view.createGameContainer(playerId);

  const rows = 4; // TODO base it on number of cards
  let currentRowNumber;
  let currentRow;
  for (const card of cards) {
    const rowNumber = Math.floor((card.id - 1) / rows);

    if (rowNumber !== currentRowNumber) {
      currentRowNumber = rowNumber;
      currentRow = view.addRowToGame();
    }

    const cardElement = view.createCardElement(card, onCardClick);
    currentRow.appendChild(cardElement);
  }

  if (DEBUG) {
    view.addTurnTable();
  }

  handleNextTurn(context.nextTurn);
}

function onCardsTurned(context) {
  const { card1, card2, matched } = context;

  disableAllCards();

  if (matched) {
    for (const card of cards) {
      if ([card1.id, card2.id].includes(card.id)) {
        card.selected = true;
      }
    }
  }

  view.showCard(card1, matched);
  view.showCard(card2, matched);

  if (DEBUG) {
    view.addMoveToTurnTable(context, onMoveResetClick);
  }

  setTimeout(() => {
    handleNextTurn(context.nextTurn);
  }, CARD_REVEAL_TIME);
}

function onMoveResetClick(clickEvent) {
  const moveElement = clickEvent.target;
  const moveId = moveElement.id;

  send("revertToMove", { id: gameId, moveId });
}

function onCardClick(clickEvent) {
  if (selectedCards.length >= 2) {
    console.log('you cannot select more cards');
    return;
  }

  const cardElement = clickEvent.target;
  const cardId = parseInt(cardElement.id);

  if (selectedCards[0] === cardId) {
    console.log('you cannot select the same card twice');
    return;
  }

  cardElement.classList.remove("bg-primary");
  cardElement.classList.add("bg-info");

  const card = cards.find(card => card.id === cardId);
  cardElement.innerHTML = card.value;
  selectedCards.push(cardId);

  if (selectedCards.length === 2) {
    const [card1, card2] = selectedCards;
    send("turnCards", { id: gameId, playerId, card1,card2 });
    selectedCards.length = 0;
  }

  cardElement.removeEventListener("click", onCardClick);
}

function onPlayerJoined(context) {
  if (context.playerId !== playerId) {
    view.addPlayerToWaitingPlayerList(context.playerId);
  } else {
    view.addMessage(null, "Waiting for player to start game", "info");
  }
}

function onGameCreated(context) {
  const { id } = context;
  if (context.playerId !== playerId) {
    const onClickFn = () => send("joinGame", { id, playerId });
    return view.addGameToList(id, onClickFn);
  }

  const onClickFn = () => send("startGame", { id, playerId });
  view.createPendingGameContainer(playerId, onClickFn);
}

function handleNextTurn(nextTurn) {
  // Just return if there are no more cards to be selected
  if (cards.filter(card => !card.selected).length === 0) {
    return;
  }

  const game = document.getElementById('game');
  view.removeMessage();

  resetAllCards();
  if (nextTurn === playerId) {
    view.addMessage(game, 'Its your turn', 'success');
  } else {
    view.addMessage(game, 'Waiting for another player to turn cards', 'info');

    disableAllCards();
  }
}

function resetAllCards() {
  for (const card of cards) {
    if (!card.selected) {
      const cardElement = document.getElementById(card.id);
      cardElement.addEventListener("click", onCardClick);
      view.turnCard(card);
    } else {
      view.disableCard(card);
    }
  }
}

function disableAllCards() {
  for (const card of cards) {
    const cardElement = document.getElementById(card.id);
    cardElement.removeEventListener("click", onCardClick);
  }
}

// WebSocket
const socket = new WebSocket(WEBSOCKET_URL);

socket.onerror = function(error) {
  console.error('error', error);
};

socket.onmessage = function(message) {
  const { event, context } = JSON.parse(message.data);
  switch (event) {
  case 'gameCreated':
    return onGameCreated(context);
  case 'playerJoined':
    return onPlayerJoined(context);
  case 'gameStarted':
    return onGameStarted(context);
  case 'cardsTurned':
    return onCardsTurned(context);
  case 'gameEnded':
    return onGameEnded(context);
  default:
    console.log(event + " not handled");
    console.log(context);
  }
};

function send(command, context) {
  socket.send(JSON.stringify({ command, context }));
}

const createGameFn = () => send("createGame", { playerId });
document.body.appendChild(view.createContainer(createGameFn));
